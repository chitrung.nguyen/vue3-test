import { mount } from '@vue/test-utils';
import TodoApp from '../../src/App.vue';

test('Renders a todo app', () => {
  const wrapper = mount(TodoApp);
  expect(wrapper.html()).toContain('TODO APP');
});

describe('Renders list todos:', () => {
  let wrapper;
  let todos;
  wrapper = mount(TodoApp);
  todos = wrapper.vm.$data.todos;

  if (todos.length === 0) {
    test('Render empty if the list todos is blank', () => {
      expect(todos.length).toBe(0);
    });
  } else {
    test('Render list todos', () => {
      expect(todos.length).toBeGreaterThan(0);
    });
  }
});

describe('Creates a todo', () => {
  test('Add new todo and render list', async () => {
    const wrapper = mount(TodoApp);
    expect(wrapper.findAll('[data-test="todo"]')).toHaveLength(0);

    await wrapper.get('[data-test="new-todo"]').setValue('New todo');
    await wrapper.get('[data-test="form"]').trigger('submit');

    expect(wrapper.findAll('[data-test="todo"]')).toHaveLength(1);
  });
});

describe('Completes a todo', () => {
  describe('Create a new todo then tick a checkbox', () => {
    let wrapper;
    beforeEach(async () => {
      wrapper = mount(TodoApp);
      await wrapper.get('[data-test="new-todo"]').setValue('New todo');
      await wrapper.get('[data-test="form"]').trigger('submit');
    });

    test('Completed a todo item', async () => {
      await wrapper.get('[data-test="todo-checkbox"]').setValue(true);
      expect(wrapper.get('[data-test="todo"]').classes()).toContain(
        'completed'
      );
    });
  });
});
